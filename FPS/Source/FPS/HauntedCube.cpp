// Fill out your copyright notice in the Description page of Project Settings.

#include "FPS.h"
#include "HauntedCube.h"


// Sets default values
AHauntedCube::AHauntedCube()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CubeRoot = CreateDefaultSubobject<USceneComponent>(TEXT("CubeRoot"));
	RootComponent = CubeRoot;

	CubeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CubeMesh"));
	CubeMesh->AttachToComponent(CubeRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	CubeTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("CubeTrigger"));
	CubeTrigger->bGenerateOverlapEvents = true;
	CubeTrigger->SetWorldScale3D(FVector(1, 1, 1));
	CubeTrigger->OnComponentBeginOverlap.AddDynamic(this, &AHauntedCube::OnPlayerTriggerRust);
	CubeTrigger->OnComponentEndOverlap.AddDynamic(this, &AHauntedCube::OnPlayerExitRust);
	CubeTrigger->AttachToComponent(CubeRoot, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	RustAmount = 1;

}

// Called when the game starts or when spawned
void AHauntedCube::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHauntedCube::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if(bRustEffectTriggered)
	{
		if (RustAmount > 0)
		{
			RustAmount -= DeltaTime;
		}
	}
	else
	{
		if (RustAmount < 1)
		{
			RustAmount += DeltaTime;
		}
	}

	UMaterialInstanceDynamic* RustMaterialInstance = CubeMesh->CreateDynamicMaterialInstance(0);

	if(RustMaterialInstance != nullptr)
	{
		RustMaterialInstance->SetScalarParameterValue(FName("RustAmount"), RustAmount);
	}
}

void AHauntedCube::OnPlayerTriggerRust(UPrimitiveComponent* OverllapedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	bRustEffectTriggered = true;

}

void AHauntedCube::OnPlayerExitRust(UPrimitiveComponent* OverllapedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bRustEffectTriggered = false;
}

