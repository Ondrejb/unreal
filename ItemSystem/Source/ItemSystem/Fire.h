// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GameFramework/Actor.h"
#include "ItemSystemCharacter.h"
#include "Fire.generated.h"

UCLASS()
class ITEMSYSTEM_API AFire : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFire();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UShapeComponent* TB_Fire;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* SM_Fire;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* PS_Fire;

	AItemSystemCharacter* CurrentPlayerController;

	UPROPERTY(EditAnywhere)
		FString HelpText = FString(TEXT("Press F to activate the fire"));

	int32 ResetTime;
	bool bPlayerIsWithinRange = false;
	bool bFireIsList = false;

	void GetPlayer(AActor* Player);
	void Light();
	void AdvanceTimer();
	void TimerHasFinished();

	FTimerHandle CountdownTimerHandle;

	UFUNCTION()
		void TriggerEnter(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void TriggerExit(UPrimitiveComponent* OverllapedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
