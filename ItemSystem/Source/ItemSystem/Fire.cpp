// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemSystem.h"
#include "Fire.h"
#include "Engine.h"


// Sets default values
AFire::AFire()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TB_Fire = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	TB_Fire->bGenerateOverlapEvents = true;
	TB_Fire->OnComponentBeginOverlap.AddDynamic(this, &AFire::TriggerEnter);
	TB_Fire->OnComponentEndOverlap.AddDynamic(this, &AFire::TriggerExit);
	TB_Fire->SetRelativeScale3D(FVector(3, 3, 1));

	RootComponent = TB_Fire;

	SM_Fire = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Fire mesh"));
	SM_Fire->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	PS_Fire = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Fire Particles"));
	PS_Fire->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	PS_Fire->SetVisibility(false);

	ResetTime = 10;
}

// Called when the game starts or when spawned
void AFire::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AFire::AdvanceTimer, 1, true);
	PS_Fire->SetVisibility(false);
}

// Called every frame
void AFire::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if(CurrentPlayerController != nullptr)
	{
		if (CurrentPlayerController->bIsInteracting && bPlayerIsWithinRange && CurrentPlayerController->Inventory.Contains("Matches") && !bFireIsList)
		{
			Light();
		}
	}
}

void AFire::GetPlayer(AActor* Player)
{
	CurrentPlayerController = Cast<AItemSystemCharacter>(Player);
}

void AFire::Light()
{
	auto xpGained = FMath::RandRange(10, 100);
	GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, FString::Printf(TEXT("You light the fire and get %d XP"), xpGained));
	CurrentPlayerController->Inventory.RemoveSingle("Matches");
	bFireIsList = true;
	PS_Fire->SetVisibility(true);
}

void AFire::AdvanceTimer()
{
	if(bFireIsList)
	{
		if(--ResetTime < 1)
		{
			TimerHasFinished();
		}
	}
}

void AFire::TimerHasFinished()
{
	PS_Fire->SetVisibility(false);
	bFireIsList = false;
	ResetTime = 10;
}

void AFire::TriggerEnter(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GetPlayer(OtherActor);
	bPlayerIsWithinRange = true;

	if (CurrentPlayerController->Inventory.Contains("Matches"))
	{
		if(!bFireIsList)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, HelpText);
		}
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Green, TEXT("You need matches to light the fire"));
	}
}

void AFire::TriggerExit(UPrimitiveComponent* OverllapedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	bPlayerIsWithinRange = false;
}

